Pod::Spec.new do |spec|
    spec.name                    = 'TrustAuditSDK'
    spec.version                 = '2.2.18'
    spec.summary                 = 'This library is responsible for creating audits.'
    spec.license                 = { :type => 'MIT', :file => 'LICENSE' }
    spec.homepage                = 'https://gitlab.com/trustchile/ios-public-frameworks/trustaudit-xcframework'
    spec.author                  = { 'Benjamín Cáceres' => 'bcaceres@trust.lat' }
    spec.source                  = { :git => 'https://gitlab.com/trustchile/ios-public-frameworks/trustaudit-xcframework.git', :tag => "#{spec.version}" }
    spec.platform                = :ios
    spec.ios.deployment_target   = '13.0'
    spec.swift_version           = '5'
    spec.requires_arc            = true
    spec.frameworks              = 'UIKit', 'CoreData'
    spec.resources               = 'Model/Audit.xcdatamodeld'
    spec.resource_bundles        = { 'TrustAudit' => ['Model/Audit.xcdatamodeld'] }
	spec.vendored_frameworks     = 'TrustAuditSDK.xcframework'
end
