# Trust Technologies
![image](https://avatars2.githubusercontent.com/u/42399326?s=200&v=4)

# Trust Audit
Trust is a platform that allows building trust and security between people and technology.
Trust-Audit allows you to send custom or automatic audit per device.

# Implementation
To implement and use the following library you need to previously install [TrustIdentify](https://github.com/trusttechnologies/lat_trust_mobile_ios_trust-identify_library).
Add in your podfile:

``` ruby
platform :ios, '13.0'

target 'AppName' do
use_frameworks!
...

	pod 'TrustAudit', :git => 'https://gitlab.com/trustchile/ios-public-frameworks/trustaudit-xcframework.git', :tag => '2.0.0'

...

end
```

And execute:

```
pod install
```
# Initialize

If you need to start a main audit instance and set the following to your AppDelegate didFinishLaunchingWithOptions:
```swift
import TrustIdentify
import TrustAudit

extension AppDelegate {
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

       let serviceName = "exampleServiceName"  
       let accessGroup = "example.access.group"  
       let clientID = "example890348h02758a5f8bee4ba2d7880a48d0581e9efb"  
       let clientSecret = "example8015437e7a963dacaa778961f647aa37f6730bd"  
      
    TrustAudit.shared.set(serviceName: serviceName, accessGroup: accessGroup) // Set access group and service name for Keychain storage  
    TrustAudit.shared.set(currentEnvironment: .prod) // Set environment  
    TrustAudit.shared.createAuditClientCredentials(clientID: clientID, clientSecret: clientSecret)  
 
       return  true
   }
}
```

To generate a new audit you need activate **Access WiFi Information** in **Signing & Capabilities**.

Once the *TrustID* is created you can use the next login AuditDataManager example:

```swift
import TrustAudit
import TrustDeviceInfo
import CoreTelephony
import SystemConfiguration.CaptiveNetwork

// MARK: - AuditDataManagerProtocol
protocol AuditDataManagerProtocol: AnyObject {
    func createLoginAudit()
}

// MARK: - AuditDataManager
class AuditDataManager: NSObject, AuditDataManagerProtocol {
    func createLoginAudit() {
        guard let savedTrustId = TrustIdentify.shared.getTrustID() else { return }
        TrustAudit.shared.createAudit(
            trustID: savedTrustId, 
            connectionType: connectionType, 
            connectionName: ssidConnection, 
            type: "trust identify", 
            result: "success", 
            method: "createLoginAudit", 
            operation: "login",
            userDni: "123456789",
            userEmail: "example@example.com",
            userPhone: "99999999",
            userFirstName: "FirstName",
            userLastName: "LastName"
        )
    }
}
```

You can create an audit (without resultObject) with the following code:
```swift
func createAudit(trustID: String, connectionType: String, connectionName: String, type: String, result: String, method: String, operation: String, userDni: String, userEmail: String, userPhone: String, userFirstName: String, userLastName: String)
```

You can create an audit (considering resultObject) with the following code:
```swift
func createAudit<T: NSObject & Codable>(trustID: String, connectionType: String, connectionName: String, type: String, result: String, method: String, operation: String, resultObject: T?)
```

#### Parameters
- **trustID**: Trust id from TrustIdentify.  
- **connectionType**: Kind of connection type (3g, 4g, wifi).  
- **connectionName**: SSID name.  
- **type**: Audit type.  
- **result**: Audit result (succes, faiulre).  
- **method**: Is the current method who call createAudit.  
- **operation**: Audit type operation.  
- ***optional* userDni**: User DNI (for example RUT).
- ***optional* userEmail**: User e-mail address.
- ***optional* userPhone**: User phone number.
- ***optional* userFirstName**: User first name.
- ***optional* userLastName**: User last name.
- ***optional* resultObject**: Optional Object, if you need to add a result object follow the previous example.   
  
# Optional Method

If you need to change the environment between production and test you must use the following method:

- TrustAudit.shared.set(currentEnvironment: .prod)
- TrustAudit.shared.set(currentEnvironment: .test)

# Permissions

To ensure the proper functioning of the audit library, in signing and Capabilities layer we must add the "Access WiFi Information" capability.

![Image](./Resources/access-wifi-information.png)

*NOTE: Remember to generate the TrustID with the [TrustIdentify](https://gitlab.com/trustchile/movil-libs/lat_trust_mobile_ios_identify-v2_library) library.*

To get location coordinates we need add in your "Info.plist" the next lines and implement an [location permissions flux](https://developer.apple.com/documentation/corelocation/requesting_authorization_for_location_services).

![Image](./Resources/location-permissions.png)
